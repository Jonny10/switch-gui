﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BoolSwitchGUI : Editor
{

    const float height = 10f;
    const float width = 30f;
    const float tackExtrusion = 1f;

    //static Color onColor = new Color(99f / 255f, 138f / 255f, 124f / 255f); //Staggart
    static Color onColor = new Color(62f / 255f, 95f / 255f, 150f / 255f); //Unity
    static Color fillColor = onColor * 0.66f;
    static float offBrightness = 0.33f;
    static Color offColor = new Color(offBrightness, offBrightness, offBrightness, 1f);

    static GUIStyle _textStyle;
    static GUIStyle textStyle
    {
        get
        {
            if(_textStyle == null)
            {
                _textStyle = new GUIStyle(EditorStyles.miniLabel);
                _textStyle.fontSize = 9;
                _textStyle.alignment = TextAnchor.MiddleLeft;
            }
            return _textStyle;
        }
    }

    static GUIStyle _backGroundStyle;
    static GUIStyle backGroundStyle
    {
        get
        {
            if(_backGroundStyle == null)
            {
                _backGroundStyle = new GUIStyle(EditorStyles.miniTextField);
                _backGroundStyle.fixedWidth = width;
                _backGroundStyle.fixedHeight = height;
            }
            return _backGroundStyle;
        }
    }

    private static bool Draw(bool value)
    {
        fillColor = new Color(62f / 255f, 95f / 255f, 150f / 255f);
        onColor = new Color(76f / 255f, 126f / 255f, 255f / 255f);

        Rect rect = GUILayoutUtility.GetLastRect();
        float prefix = EditorGUIUtility.labelWidth;
        rect.x += prefix;
        rect.y += 2f;

        //Background functions as a button
        value = (GUI.Toggle(rect, value, "", backGroundStyle));

        //Fill with color when enabled
        Rect fillrect = new Rect(rect.x + 1, rect.y + 1, (value ? width : 0), height - 2);
        if (value == true) EditorGUI.DrawRect(fillrect, fillColor);

        //Shift tack from left to right
        Rect tackRect = new Rect(rect.x + (value ? width / 2f + 1f : 0f), rect.y - tackExtrusion, width / 2f, height + tackExtrusion + 1f);
        EditorGUI.DrawRect(tackRect, (value ? onColor : offColor));

        textStyle.padding = new RectOffset((value ? 19 : 4), 0, -2, 0);
        //GUI.Label(new Rect(rect.x, rect.y, width, height), value ? "ON" : "OFF", textStyle);
        //GUI.Label(new Rect(rect.x, rect.y, width, height), "≡", textStyle);

        return value;
    }

    public static bool Draw(bool value, string text)
    {
        using (new EditorGUILayout.HorizontalScope())
        {
            EditorGUILayout.PrefixLabel(text);

            value = BoolSwitchGUI.Draw(value);
        }

        return value;
    }

    public static bool Draw(bool value, GUIContent content)
    {
        using (new EditorGUILayout.HorizontalScope())
        {
            EditorGUILayout.PrefixLabel(content);

            value = BoolSwitchGUI.Draw(value);
        }

        return value;
    }


}
