﻿using UnityEngine;
using UnityEditor;

public class Window : EditorWindow
{
    string myString = "Hello World";
    bool groupEnabled;
    static bool myBool = true;
    static bool myBool2 = false;
    float myFloat = 1.23f;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/My Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        Window window = (Window)EditorWindow.GetWindow(typeof(Window));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);

        myBool = EditorGUILayout.Toggle("Toggle", myBool);

        myBool = BoolSwitchGUI.Draw(myBool, "Option");
        myBool = BoolSwitchGUI.Draw(myBool, "Option 2");
    }

    // Add preferences section named "My Preferences" to the Preferences Window
    [PreferenceItem("Preferences")]
    public static void PreferencesGUI()
    {
        GUILayout.Label("Settings", EditorStyles.boldLabel);

        EditorGUIUtility.labelWidth = 100f;

        EditorGUILayout.Toggle("Toggle", myBool);
        myBool = BoolSwitchGUI.Draw(myBool, "Toggle");
        EditorGUILayout.Toggle("Toggle", false);

        myBool2 = BoolSwitchGUI.Draw(myBool2, "Toggle 2");
        BoolSwitchGUI.Draw(false, "Toggle 3");
        BoolSwitchGUI.Draw(false, "Toggle 4");
        EditorGUILayout.Toggle("Toggle", myBool);

    }
}